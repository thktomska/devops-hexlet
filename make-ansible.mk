ansible-vaults-encrypt:
	ansible-vault encrypt ansible/development/group_vars/all/vault.yml

ansible-vaults-decrypt:
	ansible-vault decrypt ansible/development/group_vars/all/vault.yml

ansible-vaults-edit:
	ansible-vault edit ansible/development/group_vars/all/vault.yml
