app-build:
	docker-compose build

app:
	docker-compose up

app-down:
	docker-compose down

app-bash:
	docker-compose run app bash

app-install:
	docker-compose run app bundle

app-db-drop:
	docker-compose run app rails db:drop

app-db-prepare:
	docker-compose run app rails db:create
	docker-compose run app rails db:migrate
