U := ubuntu
# E := development

production-setup:
	ansible-playbook ansible/site.yml -i ansible/production -u $U --ask-vault-pass

production-env-update:
	ansible-playbook ansible/site.yml -i ansible/production -u $U --tag env --ask-vault-pass

production-deploy:
	ansible-playbook ansible/deploy.yml -i ansible/production -u $U --ask-vault-pass

